<?php

/**
 * @copyright  jedoLabs 2018 <https://jedo-labs.de>
 * @author     Jens Doberenz (WurzelGnOOm)
 * @package    jedolabs/fontawesome-icon-picker-bundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/jedolabs/fontawesome-iconpicker-bundle
 *
 */


if (TL_MODE == 'BE')
{
    $GLOBALS['TL_CSS'][] = 'bundles/contaofontawesomewidget/css/FontAwesomeWidget.min.css|static';
    $GLOBALS['TL_JAVASCRIPT'][] = 'bundles/contaofontawesomewidget/js/FontAwesomeWidget.min.js';


    $GLOBALS['TL_JAVASCRIPT'][] = JedoLabs\ContaoFontawesomeWidgetBundle\ContaoFontawesomeWidgetBundle::getFontAwesomeJavascript();
}

if($GLOBALS['TL_CONFIG']['FIP_FontAwesomeSRC'])
{
    $GLOBALS['BE_FFL']['FontAwesomeWidget'] = 'JedoLabs\ContaoFontawesomeWidgetBundle\ContaoBackendWidget\FontAwesomeWidget';
}