<?php

/**
 * @copyright  jedoLabs 2018 <https://jedo-labs.de>
 * @author     Jens Doberenz (WurzelGnOOm)
 * @package    jedolabs/fontawesome-icon-picker-bundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/jedolabs/fontawesome-iconpicker-bundle
 *
 */

namespace JedoLabs\ContaoFontawesomeWidgetBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use JedoLabs\ContaoFontawesomeWidgetBundle\ContaoFontawesomeWidgetBundle;

/**
 * Plugin for the Contao Manager.
 *
 * @author Jens Doberenz <https://github.com/jedoStyle>
 */
class Plugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ContaoFontawesomeWidgetBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
